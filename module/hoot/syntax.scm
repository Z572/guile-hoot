;;; Syntax primitives
;;; Copyright (C) 2024 Igalia, S.L.
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Primitive syntax.
;;;
;;; Code:

(library (hoot syntax)
  (export _
          ... => else
          lambda case-lambda
          define define-values let let* letrec letrec* let-values let*-values
          or and
          begin 
          if cond case when unless
          do
          set!
          quote quasiquote unquote unquote-splicing
          include include-ci
          define-syntax let-syntax letrec-syntax
          syntax-rules syntax-error

          include-from-path
          define-syntax-rule
          syntax-case syntax quasisyntax unsyntax unsyntax-splicing
          syntax->datum datum->syntax
          identifier? generate-temporaries free-identifier=? bound-identifier=?
          with-syntax identifier-syntax syntax-local-binding
          syntax-violation procedure-property
          target-runtime
          gensym
          lambda* case-lambda* define*)
  (import (hoot primitives)))
