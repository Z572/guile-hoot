;;; Multiple values
;;; Copyright (C) 2024 Igalia, S.L.
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Multiple values.
;;;
;;; Code:

(library (hoot values)
  (export call-with-values (rename %values values))
  (import (only (hoot primitives) %values %call-with-values apply)
    (hoot syntax))

  (define-syntax call-with-values
    (lambda (stx)
      (syntax-case stx (lambda)
        ((_ producer (lambda args body0 body ...))
         #'(%call-with-values producer (lambda args body0 body ...)))
        (id (identifier? #'id)
            #'(lambda (producer consumer)
                (let ((p producer) (c consumer))
                  (%call-with-values p (lambda args (apply c args))))))))))
